# -*- coding:utf-8 -*-

# 2D turing machines in which
# move directions are given by numbers
# on the num pad

# Therefore,
# directions:
#       7 8 9
#       4   6
#       1 2 3
# operations:
#   5   increment
#   0   decrement
#   +   repeat from last - 
#           unless current cell has value 0
#   -   linked to +
#   *   prints ascii value of current cell

import re

def movePointer(pointer, direction):
    if direction in (7,8,9):
        pointer[1] += 1
        return pointer
    if direction in (1,2,3):
        pointer[1] -= 1
        return pointer
    if direction in (3,6,9):
        point[0] += 1
        return pointer
    if direction in (1,4,7):
        pointer[0] -= 1
        return pointer

def addToCell(memory, position, value):
    memory[tuple(position)] = valueInCell(memory, tuple(position)) + value
    return valueInCell(memory, position)

def valueInCell(memory, position):
    return memory.get(tuple(position), 0)

def formatSource(source):
    # Make use of the fact that strings are immutable
    return re.sub('[^1234567890\-+*]', '', source)

def isInt(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

def run(Source):
    # Memory maps a point to a value
    memory = {(0,0):0}
    # Pointer points to current (x,y) position
    pointer = [0,0]

    # Position in source
    position = -1 # HACK: Start at 0

    # Modifiable copy of source
    # all non-valid chars are removed
    source = formatSource(Source)

    # Run until out of commands
    while(position < len(source) - 1):
        position += 1
        command = source[position]

        if isInt(command):
            command = int(command)
            if command not in (5,0):
                movePointer(pointer, int(command))
            else:
                if command == 5:
                    addToCell(memory, pointer, 1)
                elif command == 0:
                    addToCell(memory, pointer, -1)
        else:
            if command == '+':
                # If the value at the current cell is 0, ignore
                if valueInCell(memory, pointer) != 0:
                    # Go back until we find a -
                    # Throw error if none found
                    while source[position] != '-':
                        position -= 1
                        if position < 0:
                            raise Exception('Could not find matching -!')
            elif command == '*':
                # Print ascii of corresponding value
                print str(unichr(valueInCell(memory, pointer)))
                