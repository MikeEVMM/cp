# -*- coding:utf-8 -*-

centenasDict = {
    1:'cento',
    2:'duzentos',
    3:'trezentos',
    4:'quatrocentos',
    5:'quinhentos',
    6:'seiscentos',
    7:'setcentos',
    8:'oitocentos',
    9:'novecentos'
}

dezenasDict = {
    2:'vinte',
    3:'trinta',
    4:'quarenta',
    5:'cinquenta',
    6:'sessenta',
    7:'setenta',
    8:'oitenta',
    9:'noventa'
}

unidadesDict = {
    1:'um',
    2:'dois',
    3:'três',
    4:'quatro',
    5:'cinco',
    6:'seis',
    7:'sete',
    8:'oito',
    9:'nove'
}

especiaisDict = {
    10:'dez',
    11:'onze',
    12:'doze',
    13:'treze',
    14:'catorze',
    15:'quinze',
    16:'dezasseis',
    17:'dezassete',
    18:'dezoito',
    19:'dezanove'
}

def lerTresCasas(numero):
    extenso = ''
    if int(numero[0]) > 0:
        if int(numero[0]) == 1 and int(numero[1]) == 0 and int(numero[2]) == 0:
            extenso += 'cem '
        else:
            extenso += centenasDict[int(numero[0])] + ' '
    if int(numero[1]) > 0:
        if int(numero[0]) > 0:
            extenso += 'e '
        if int(numero[1:3]) in especiaisDict.keys():
            extenso += especiaisDict[int(numero[1:3])] + ' '
        else:
            if int(numero[1]) > 0:
                extenso += dezenasDict[int(numero[1])] + ' '
                if int(numero[2]) > 0:
                    extenso += 'e '
    if int(numero[2]) > 0 and int(numero[1:3]) not in especiaisDict.keys():
        if not (int(numero[0]) == 0 and int(numero[1]) == 0 and int(numero[2]) == 1):
            extenso += unidadesDict[int(numero[2])] + ' '
    return extenso

def extenso(Numero):
    numero = str(Numero)

    # Fix to 6 houses
    while len(numero) < 6:
        numero = '0' + numero
    
    extenso = ''
    if int(numero[:3]) > 0:
        extenso += lerTresCasas(numero[:3])
        extenso += 'mil '
    if int(numero[3:]) > 0:
        if extenso != '':
            extenso += 'e '
        extenso += lerTresCasas(numero[3:])
    
    return extenso

print extenso(input('Insira numero a ler: '))
