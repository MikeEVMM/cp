# -*- coding:utf-8 -*-

def primo(x):
    for i in range(2,x):
        if x%i == 0:
            return False
    return True

for i in range(2, 1001):
    if primo(i):
        print i