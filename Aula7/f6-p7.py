# -*- coding:utf-8 -*-

import math

epsilon = 10**(-5)
function = lambda x: math.log(x) + x**(-2) - 1

print('''Suggested value for a: 1.4
Suggested value for b: 2.4''')
a = input('Insert a: ') * 1.
b = input('Insert b: ') * 1.

if function(a) * function(b) > 0:
    print 'Cannot find roots in this interval.'
    exit()

z = (a+b)/2
while abs(function(z)) > epsilon:
    if function(z) * function(a) < 0:
        b = z
    else:
        a = z
    z = (a+b)/2

print function(z)
print 'f({}) ~ 0'.format(z)