# -*- coding: utf-8 -*-

def factorial(x):
    result = 1
    for i in range(1, x+1):
        result *= i
    return result

def binomial(n, m):
    return factorial(n)/(factorial(m)*factorial(n-m))*(1./2)**n

n = input('n: ')
m = input('m: ')

print 'Probability of {} throws and {} Heads: {}/1'.format(n, m, binomial(n, m))