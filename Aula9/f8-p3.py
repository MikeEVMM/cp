# -*- coding:utf-8 -*-

class Rational:
  def __init__(self, n=1, d=1):
    self.n = n
    self.d = d
  
  def __repr__(self):
    return '{}/{}'.format(self.n, self.d)

  def num(self):
    return self.n * 1.0 / self.d

  def fromInput(self, msg=''):
    return self.fromString(raw_input(msg))

  def fromString(self, string):
    sepIndex = string.find('/')
    self.n = int(string[:sepIndex])
    self.d = int(string[sepIndex+1:])
    return self.reduce()
  
  @staticmethod
  def GCD(a,b):
    a = float(a)
    b = float(b)
    # using Binary GCD algorithm
    if a==b:
      return a
    if a==0:
      return b
    if not a%2 and not b%2:
      return Rational.GCD(a/2,b/2)
    if not a%2 and b%2:
      return Rational.GCD(a/2,b)
    if a%2 and not b%2:
      return Rational.GCD(a,b/2)
    # At this point, both are odd
    if a > b:
      return Rational.GCD((a-b)/2,b)
    return Rational.GCD((b-a)/2,a)

  def reduce(self):
    gcd = Rational.GCD(abs(self.n), abs(self.d))
    self.n /= gcd
    self.d /= gcd
    self.n = int(self.n)
    self.d = int(self.d)
    return self

  def __add__(self, other):
    toAdd = Rational()
    if type(other) is type(self):
      toAdd = other
    elif type(other) is int:
      toAdd.n = other
    elif type(other) is float:
      raise ValueError('Cannot add floats to Rational type')
    return Rational(self.n + toAdd.n, self.d + toAdd.d).reduce()

  def __mul__(self, other):
    toMul = Rational()
    if type(other) is type(self):
      toMul = other
    elif type(other) is int:
      toMul.n = other
    elif type(other) is float:
      raise ValueError('Cannot multiply floats and Rational type')
    return Rational(self.n * toMul.n, self.d * toMul.d).reduce()

  def __sub__(self, other):
    toSub = Rational()
    if type(other) is type(self):
      toSub = other
    elif type(other) is int:
      toSub.n = other
    elif type(other) is float:
      raise ValueError('Cannot subtract floats and Rational type')
    return Rational(self.n - toSub.n, self.d - toSub.d).reduce()
    
  def __div__(self, other):
    toDiv = Rational()
    if type(other) is type(self):
      toDiv = other
    elif type(other) is int:
      toDiv.n = other
    elif type(other) is float:
      raise ValueError('Cannot divide floats and Rational type')
    return Rational(self.n * toDiv.d, self.d * toDiv.n).reduce()
   
  def __rdiv__(self, other):
    toDiv = Rational()
    if type(other) is type(self):
      toDiv = other
    elif type(other) is int:
      toDiv.n = other
    elif type(other) is float:
      raise ValueError('Cannot divide floats and Rational type')
    return Rational(self.d * toDiv.n, self.n * toDiv.d).reduce()
 
  def __neg__(self):
    if self.num() < 0:
      return Rational(abs(self.n), abs(self.d))
    return Rational(-self.n, self.d).reduce()

  def __pow__(self,arg):
    return Rational(self.n**arg, self.d**arg).reduce()


# --- TEST CASES ---
a = Rational(3,5)
print 'a'
print a
print a.num()

print 'Define b:'
b = Rational().fromInput()
print b
print b.num()

print 'a+b'
print a+b

print 'a*b'
print a*b

print 'a/b'
print a/b

print '1/a'
print 1/a 

print '-a'
print -a

print 'a**2'
print a**2
