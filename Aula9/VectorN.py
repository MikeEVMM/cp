# -*- coding:utf-8 -*-

class VecN:
  def dimension(self):
    return len(self.components)
  
  def fromList(self, inList):
    self.components = []
    for i in range(len(inList)): 
      if type(inList[i]) not in (int, float):
        raise ValueError('Components must be numbers.') 
      self.components.append(inList[i])
    return self
  
  def norm(self):
    return (sum([comp**2 for comp in self.components])**0.5)

  def __init__(self, *args):
    self.components = []
    if len(args) > 0 and type(args[0]) in (list,tuple):
      self.fromList(args[0])
    else:
      for i in range(len(args)): 
          if type(args[i]) not in (int, float):
            raise ValueError('Components must be numbers.') 
          self.components.append(args[i])

  def __repr__(self):
    return 'Vector({})'.format(str(self.components)[1:-1])

  def __add__(self,other):
    if type(other) is not type(self):
      raise ValueError('Cannot add Vector to non-vector.')
    if other.dimension() != self.dimension():
      print 'Cannot add vectors of different dimension.'
      return None
    result = []
    for i in range(len(other.components)):
      result.append(self.components[i]+other.components[i])
    return VecN().fromList(result)

  def __neg__(self):
    return VecN([-self.components[i] for i in range(len(self.components))])

  def __sub__(self, other):
    if type(other) is not type(self):
      raise ValueError('Cannot subtract Vector to non-vector.')
    if other.dimension() != self.dimension():
      print 'Cannot subtract vectors of different dimension.'
      return None
    return VecN([self.components[a] - other.components[a] for a in range(self.dimension())])

  def __mod__(self, other):
    if type(other) is not type(self):
       raise ValueError('Cannot apply dot product between vector and non-vector.')
    if other.dimension() != self.dimension():
       print 'Cannot apply dot product to vectors of different dimension.'
       return None
    dot = 0
    for i in range(self.dimension()):
      dot += self.components[i] * other.components[i]
    return dot

  def __div__(self, other):
    if type(other) not in (float, int):
      raise ValueError('Cannot divide vector by non-number.')
    other = float(other)
    return VecN([self.components[i] / other for i in range(self.dimension())])

  def __lt__(self, other):
    return self.norm() < other.norm()

  def __le__(self, other):
    return self.norm() <= other.norm()

  def __eq__(self, other):
    for i in range(self.dimension()):
      if self.components[i] != other.components[i]:
        return False
    return True

  def __ne__(self, other):
    return not (self==other)

  def __ge__(self, other):
    return self.norm() >= other.norm()

  def __gt__(self, other):
    return self.norm() > other.norm()
