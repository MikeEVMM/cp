# -*- coding:utf-8 -*-

import math

n=0
a=1
b=1
while a!=0:
    n = n+1
    b = 2.0**(-n)
    a = 1.0 + 2.0**(-n) - 1.0
print 'Numero de algarismos significativos: ', abs(math.round(math.log10(b)))