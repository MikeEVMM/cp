# -*- coding:utf-8 -*-

class Vec:

  def __init__(self,x=0,y=0,z=0):
    self.x=x
    self.y=y
    self.z=z

  def fromInput(self, msg=''):
    given = input(msg)
    self.x = given[0]
    self.y = given[1]
    self.z = given[2]

  def norm(self):
    return (self.x**2+self.y**2+self.z**2)**0.5

  def prod(self,other):
    return Vec(self.y*other.z-self.z*other.y, self.z*other.x-self.x*other.z, self.x*other.y-self.y*other.x)

  def __mul__(self,other):
    return self.prod(other)

  def __repr__(self):
    return "(%f,%f,%f)" % (self.x, self.y, self.z)

  def __add__(self, other):
    return Vec(self.x + other.x, self.y + other.y, self.z + other.z)

  def __sub__(self, other):
    return Vec(self.x - other.x, self.y - other.y, self.z - other.z)

  def __mod__(self, other):
    return self.x * other.x + self.y * other.y + self.z * other.z

  def __div__(self, other):
    if type(other) not in (int,float):
       raise ValueError('Vector can only divide by number.')
    other = float(other)
    return Vec(self.x/other, self.y/other, self.z/other)

  def __neg__(self):
    return Vec(-self.x, -self.y, -self.z)

  def __lt__(self, other):
    return self.norm() < other.norm()

  def __le__(self, other):
    return self.norm() <= other.norm()

  def __eq__(self, other):
    return self.x == other.y and self.y == other.y and self.z == other.z

  def __ne__(self, other):
    return self.x != other.x or self.y != other.y or self.z != other.z

  def __ge__(self, other):
    return self.norm() >= other.norm()

  def __gt__(self, other):
    return self.norm() > other.norm()


A = Vec()
B = Vec()

A.fromInput('Vector A: ')
B.fromInput('Vector B: ')

print 'Representation of vector A:',
print A

print 'Representation of vector B:',
print B

print 'A + B:',
print A+B

print 'A - B:',
print A-B

print 'A (scalar) B:',
print A%B

print 'A/a:',
a = input('a = ')
print A/a

print '-A:',
print -A

print '(norm)A:',
print A.norm()

print 'Comparison:'
print 'A > B, A >= B, A == B, A <= B, A < B, A != B'
print A>B, A>=B, A==B, A<=B, A<B, A!=B
