# -*- coding:utf-8 -*-

import re

class Date:
    weekdays = ['Monday', 'Tuesday', 'Wedensday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

    def __init__(self, day = 1, month = 1, year = 1600):
        self.day = day
        self.month = month
        self.year = year
    
    @staticmethod
    def isLeapYear(year):
        if (not year%4) and (year%100 or not year%400):
            return True

    @staticmethod
    def getFebDaysByYear(year):
        if Date.isLeapYear(year):
            return 29
        else:
            return 28
    
    @staticmethod
    def getMonthDays(month):
        if month == 2:
            raise ValueError('Please use getFebDaysByYears for february!')
        if month < 8:
            if not month%2: # Before june, even months have 30 days
                return 30
            else:
                return 31
        else:
            if not month%2: # After june, even months have 31 days
                return 31
            else:
                return 30

    @staticmethod
    def monthStrToNum(monthStr):
        monthStr = monthStr.lower()
        try:
            return ['jan','feb','mar','apr','may','jun','jul','aug','set','oct','nov','dec'].index(monthStr) + 1
        except:
            return -1

    def valid(self):
        date = self.day, self.month, self.year
        for arg in date:
            if type(arg) is not int:
                return False
        if date[0] < 1:
            return False
        if date[1] > 12:
            return False
        if date[2] < 1:
           return False
        elif date[2] < 1600:
            print "https://xkcd.com/376/"
            return False
        if date[1] == 2:
            if date[0] > Date.getFebDaysByYear(date[2]):
                return False
        else:
            if date[0] > Date.getMonthDays(date[1]):
                return False
        return True

    def previous(self):
        day,month,year = self.day,self.month,self.year
        day -= 1
        if day < 1:
            month -= 1
            if month < 1:
                year -= 1
                month = 12

            if month == 2:
                day = Date.getFebDaysByYear(year)
            else:
                day = Date.getMonthDays(month)
        return Date(day,month,year)
    
    def next(self):
        day,month,year = self.day,self.month,self.year
        day += 1
        monthDays = Date.getFebDaysByYear(year) if month == 2 else Date.getMonthDays(month)
        if day > monthDays:
            month += 1
            if month > 12:
                year += 1
                month = 1
            day = 1
        return Date(day,month,year)
    
    def diff(self,other):
        start = self
        end = other
        dif = 0
        if end < start:
            start,end = end,start
        while start.year < end.year - 1:
            if Date.isLeapYear(start.year):
                dif += 366
            else:
                dif += 365
            start.year += 1
        
        while start != end:
            dif += 1
            start = start.next()
        
        return dif

    def weekday(self):
        # 1/8/2016 is/was a monday
        return Date.weekdays[self.diff(Date(1,8,2016))%7]
    
    def parse(self, datestring):
        try:
            year, month, day = re.search(r'((?:\d\d)?\d\d)/((?:\d?\d)|(?:\w\w\w))/(\d?\d)', datestring).groups()
        except:
            try:
                day, month, year = re.search(r'(\d?\d)/((?:\d?\d)|(?:\w\w\w))/((?:\d\d)?\d\d)')
            except:
                return False
        if type(month) is str:
            month = Date.monthStrToNum(month)
            if month < 0:
                return False
        year = int(year)
        day = int(day)
        if year < 30:
            year += 2000
        elif year < 100:
            year += 1900
        self.day, self.month, self.year = day, month, year
        return True
    
    def pseudoID(self):
        return self.year * 1000 + self.month * 100 + self.day

    def __repr__(self):
        if not self.valid():
            return 'Invalid date.'
        return '{}/{}/{}'.format(self.year,self.month,self.day)
    
    def __eq__(self, other):
        return self.year == other.year and self.month == other.month and self.day == other.day
    
    def __ne__(self,other):
        return self.year != other.year or self.month != other.month or self.day != other.day
    
    def __lt__(self, other):
        return self.pseudoID() < other.pseudoID()