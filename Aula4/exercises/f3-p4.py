# -*- coding: utf-8 -*-

def requestInt(message):
    newValue = input(message)
    if type(newValue) is not int:
        print 'Not an integer.'
        return requestInt(message)
    return newValue

def main():
    n = requestInt('Insert an integer n: ')

    count = 0
    sum = 0
    while count < n:
        sum += requestInt('Insert an integer: ')
        count += 1
    
    print 'Sum of all inserted integers (excluding n) is {}.'.format(sum)

main()