# -*- coding: utf-8 -*-

def requestValue():
    newValue = input('Insert value: ')
    if type(newValue) is not int:
        print 'Not an integer.'
        return requestValue()
    return newValue

def main():
    inputValues = []

    while len(inputValues) < 5:
        inputValues.append(requestValue())

    greatest = inputValues[0]
    for element in inputValues[1:]:
        if element > greatest:
            greatest = element
    
    print 'Greatest element in list is {}.'.format(greatest)
    
main()