# -*- coding: utf-8 -*-

def factorial(x):
    result = 1
    for i in range(1, x+1):
        result *= i
    return result

def probabilityOfMHeadsInNThrows(m, n):
    return factorial(n)/(factorial(m)*factorial(n-m))*(1./2)**n

print 'Probability of 100 throws and 0 Heads: {}/1'.format(probabilityOfMHeadsInNThrows(0, 100))

print 'Probability of 100 throws and exactly 50 heads: {}/1'.format(probabilityOfMHeadsInNThrows(50, 100))

print 'Attempting to determine the prob. of 5000 heads in 10000 throws:'

try:
    print probabilityOfMHeadsInNThrows(5000, 10000)
except Exception as err:
    print 'Got error', err