# -*- coding: utf-8 -*-

def requestInt(message):
    newValue = input(message)
    if type(newValue) is not int:
        print 'Not an integer.'
        return requestInt(message)
    return newValue

def main():
    n = requestInt('Insert integer n: ')
    
    if n < 1:
        print 'n must be greater than 0.'
        return main()

    inserted = []
    for i in range(0, n):
        inserted.append(requestInt('Insert an integer: '))
        
    greatest = inserted[0]
    for m in inserted[1:]:
        if m > greatest:
            greatest = m
    
    print '{} was the greatest inserted integer.'.format(greatest)

main()