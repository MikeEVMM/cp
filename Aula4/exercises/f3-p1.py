# -*- coding: utf-8 -*-

def main():
    char = input('Insert char to look for: ')
    text = input('Insert text to search in: ')

    if type(char) is not str or type(text) is not str:
        print 'Invalid inputs.'
        return main()
    
    print 'Found {} instances of {} in given text.'.format(text.count(char), char)

main()