# -*- coding: utf-8 -*-

# Declare currency object
class currency(object):
    __slots__ = ['name', 'value']
    def __init__(self, name, value):
        self.name = name
        self.value = value
    def __str__(self):
        return '(Name:{}; Value:{})'.format(self.name, self.value)
    def __repr__(self):
        return '(Name:{}; Value:{})'.format(self.name, self.value)

# Declare all possible currency
currencyList = [currency('50 eur', 5000), currency('20 eur', 2000), currency('10 eur', 1000),\
    currency('5 eur', 500), currency('1 eur', 100), currency('50 cent', 50), currency('10 cent', 10),\
    currency('5 cent', 5), currency('2 cent', 2), currency('1 cent', 1)]

# Helper function
def requireFloat(message):
    newValue = input(message)
    if not (type(newValue) is float or type(newValue) is int):
        print 'Not a float.'
        return requireFloat(message)
    return float(newValue)

# Main program
def main():
    change = -1
    while change < 0:
        pay = int(requireFloat('Insert value to pay (eur): ')*100)
        given = int(requireFloat('Ammount given (eur): ')*100)

        change = given - pay

        if change < 0:
            print 'Insufficient payment!'
    
    changeCopy = change
    changeInCurrency = []
    while changeCopy > 0:
        for currency in currencyList:
            if changeCopy >= currency.value:
                changeCopy -= currency.value
                changeInCurrency.append(currency)
                break
    
    changeInCurrency.sort()

    print 'Change: ', changeInCurrency    

main()