# -*- coding: utf-8 -*-

import apiRequests

def requestName():
    name = input('Insert food name: ')

    if type(name) is not str:
        print 'Name must be string.'
        return requestName()
    
    return name

def requestWater():
    name = input('How much water? (In ml): ')

    if not (type(name) is float or type(name) is int):
        print 'Ammount must be number.'
        return requestWater()
    
    return float(name)

def main():
    foodName = requestName()
    waterAmmount = requestWater()

    foodEnergyPerGram = apiRequests.getFoodJoule(foodName)

    if foodEnergyPerGram < 0:
        print 'Could not find food :('
        return

    waterSpecificHeat = 4.180
    waterDensity = 1.
    ambientTemperature = 20
    boilingTemperature = 100

    deltaTemperature = boilingTemperature - ambientTemperature
    energyToBoil = waterAmmount * waterDensity * waterSpecificHeat * deltaTemperature
    foodToBoilInGram = energyToBoil / foodEnergyPerGram

    print '''\
Considering:
    A constant specific heat of {}J/(ºC*g);
    A volume of water of {}ml;
    An ambient temperature of {}ºC;
    Water density of {}g/ml;
    That water boils at {}ºC;
    That {} has {} J/g;

It\'d take, energy wise,
{} grams of {} to boil {}ml of water.\
    '''.format(waterSpecificHeat, waterAmmount, ambientTemperature,\
            waterDensity, boilingTemperature, foodName, foodEnergyPerGram,\
            foodToBoilInGram, foodName, waterAmmount)

main()