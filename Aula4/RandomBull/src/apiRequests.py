import urllib
import json
from secret import foodAPIKey

reportsRequestLink = "http://api.nal.usda.gov/ndb/reports/?format=json&api_key=" + foodAPIKey

searchRequestLink = "http://api.nal.usda.gov/ndb/search/?format=json&sort=r&max=1&api_key=" + foodAPIKey

def getFoodJSON(name):
    foodRequest = searchRequestLink + "&q=" + name
    requestResult = urllib.urlopen(foodRequest)
    return json.load(requestResult)

def getFoodNDBNO(name):
    foodJson = getFoodJSON(name)
    if('errors' in foodJson):
        return -1
    return int(foodJson['list']['item'][0]['ndbno'])

def getFoodInfo(name):
    ndbno = getFoodNDBNO(name)

    if(ndbno < 0):
        return {}

    infoRequest = reportsRequestLink + "&ndbno=" + str(ndbno)
    requestResult = urllib.urlopen(infoRequest)
    return json.load(requestResult)

""" Kcal energy per gram. """
def getFoodKcal(name):
    foodReport = getFoodInfo(name)

    if foodReport == {}:
        return -1

    nutrients = foodReport['report']['food']['nutrients']
    i = 0
    while nutrients[i]['name'] != 'Energy':
        i += 1
    kcal = float(nutrients[i]['measures'][0]['value'])
    return kcal

""" Joule energy per gram. """
def getFoodJoule(name):
    kcal = getFoodKcal(name)

    if kcal < 0:
        return -1
    
    return kcal * 4184