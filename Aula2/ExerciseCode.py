print "Running code snippet A"

for i in range(10):
    for j in range(10):
        print i,'x',j,'=',i*j
   
print "Pause...."
raw_input()

print "Running code snippet B"

for i in range(1, 10):
    print i, ':',
    for j in range(1, 10):
        print i*j,
    print

print "Pause...."
raw_input()

print "Running C"

for i in range(1, 10):
    for j in range(1, i):
        print i, 'x', j, '=', i*j

print "Pause..."
raw_input()

print "Running D"
for i in range(1,10):
    for j in range(1,i):
        z= i*j
        print i,'x', j, '=', z,
        if z%2==0:
            print "par"
        else:
            print "impar"

print "Pause..."
raw_input()

print "Running E"
n = input('Introduza um valor inteiro: ')
s = 0
for i in range(n+1):
    print i,s
    s = s+i
    print i,s
print 'Resultado da soma: ', s

print "Pause..."
raw_input()

print "Running F"
n= input("Introduza um valor inteiro: ")
s=0
i=1
while i <= n:
    print i, s
    s= s+i
    i= i+1
    print i, s
print "Resultado da soma: ", s

print "Pause..."
raw_input()

print "Running G"

n= input("Introduza um valor inteiro: ")
print "o valor introduzido e ",
if n==0:
    print "zero",
elif n>0:
    print "positivo",
else:
    print "negativo",
print ":", n