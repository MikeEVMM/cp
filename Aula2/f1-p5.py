# -*- coding: utf-8 -*-
def main():
    n = input('Introduza um número inteiro: ')

    # Type checks
    if type(n) is not int:
        print 'n não é inteiro.'
        return
    
    sum = 0
    for i in range(0, n+1):
        sum = sum + 1 / factorial(i)
    
    print 'Soma: ', sum


def factorial(i):
    n = float(i)

    if n<0:
        raise ValueError('O fatorial de números negativos não está definido!')
        return i
    
    value = 1.
    while n>0:
        value = value*n
        n = n - 1
    return value

main()