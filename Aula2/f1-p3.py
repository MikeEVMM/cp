# -*- coding: utf-8 -*-
def main():
    n = input('Introduza o valor de n: ')
    k = input('Introduza o valor de k: ')

    # Check if int
    if type(n) is not int:
        print "n não é inteiro"
        return
    if type(k) is not int:
        print "k não é inteiro"
        return
    
    # Cast to float
    n = float(n)
    k = float(k)

    whole,rest = int(n/k), (n/k)%1

    print 'n/k tem resultado inteiro {} e resto {}'.format(whole, rest)

main()