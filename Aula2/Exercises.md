# Exercise A

## Code

```python
for i in range(10):
    for j in range(10):
        print i,'x',j,'=',i*j
```

## Output

```python
0 x 0 = 0
0 x 1 = 0
0 x 2 = 0
0 x 3 = 0
0 x 4 = 0
0 x 5 = 0
0 x 6 = 0
0 x 7 = 0
0 x 8 = 0
0 x 9 = 0
1 x 0 = 0
1 x 1 = 1
1 x 2 = 2
1 x 3 = 3
1 x 4 = 4
1 x 5 = 5
1 x 6 = 6
1 x 7 = 7
1 x 8 = 8
1 x 9 = 9
2 x 0 = 0
2 x 1 = 2
2 x 2 = 4
2 x 3 = 6
2 x 4 = 8
2 x 5 = 10
2 x 6 = 12
2 x 7 = 14
2 x 8 = 16
2 x 9 = 18
3 x 0 = 0
3 x 1 = 3
3 x 2 = 6
3 x 3 = 9
3 x 4 = 12
3 x 5 = 15
3 x 6 = 18
3 x 7 = 21
3 x 8 = 24
3 x 9 = 27
4 x 0 = 0
4 x 1 = 4
4 x 2 = 8
4 x 3 = 12
4 x 4 = 16
4 x 5 = 20
4 x 6 = 24
4 x 7 = 28
4 x 8 = 32
4 x 9 = 36
5 x 0 = 0
5 x 1 = 5
5 x 2 = 10
5 x 3 = 15
5 x 4 = 20
5 x 5 = 25
5 x 6 = 30
5 x 7 = 35
5 x 8 = 40
5 x 9 = 45
6 x 0 = 0
6 x 1 = 6
6 x 2 = 12
6 x 3 = 18
6 x 4 = 24
6 x 5 = 30
6 x 6 = 36
6 x 7 = 42
6 x 8 = 48
6 x 9 = 54
7 x 0 = 0
7 x 1 = 7
7 x 2 = 14
7 x 3 = 21
7 x 4 = 28
7 x 5 = 35
7 x 6 = 42
7 x 7 = 49
7 x 8 = 56
7 x 9 = 63
8 x 0 = 0
8 x 1 = 8
8 x 2 = 16
8 x 3 = 24
8 x 4 = 32
8 x 5 = 40
8 x 6 = 48
8 x 7 = 56
8 x 8 = 64
8 x 9 = 72
9 x 0 = 0
9 x 1 = 9
9 x 2 = 18
9 x 3 = 27
9 x 4 = 36
9 x 5 = 45
9 x 6 = 54
9 x 7 = 63
9 x 8 = 72
9 x 9 = 81

```

## Explanation

`for i in range(10):`

Loops variable `i` from `0...9` (range of `0...10`), with a delta of `1`.

`for j in range(10):`

For each `i` in `0...9` loops variable `j` from `0...9` (range of `0...10`), with a delta of `1`.

`print i, 'x', j, '=', i*j`

Prints to the console, in order and inlined:

- The value of variable `i`

- The string `x`, as denoted by the `' '` (would also work if encapsuled in `" "`)

- The value of variable `j`

- The value of the product `i*j`

In short, prints multiplication tables of 1 to 9.

---

# Exercise B

## Code

```python
for i in range(1, 10):
    print i, ':',
    for j in range(1, 10):
        print i*j,
    print
```

## Output

```python
1 : 1 2 3 4 5 6 7 8 9
2 : 2 4 6 8 10 12 14 16 18
3 : 3 6 9 12 15 18 21 24 27
4 : 4 8 12 16 20 24 28 32 36
5 : 5 10 15 20 25 30 35 40 45
6 : 6 12 18 24 30 36 42 48 54
7 : 7 14 21 28 35 42 49 56 63
8 : 8 16 24 32 40 48 56 64 72
9 : 9 18 27 36 45 54 63 72 81
```

## Explanation

`for i in range(1, 10):`

Loops variable `i` through 1...9 (range 10) with delta 1.

`print i, ':'`

Prints, for each value of `i`, and inlined (no paragraph),
value of variable `i` and the string `:` (as denoted by `' '`)

`for j in range(1, 10):`

For each value of `i`,
loops variable `j` through 1...9 (range 10) with delta 1

`print i*j,`

For each value of `j` prints the value of `i*j`, with no trailing 
paragraph -- next print is inlined.

`print`

After looping through variable `j`, prints a paragraph, before
moving onto next value of `i`.

---

# Exercise C

## Code

```python
for i in range(1, 10):
    for j in range(1, i):
        print i, 'x', j, '=', i*j
```

## Output

```python
2 x 1 = 2
3 x 1 = 3
3 x 2 = 6
4 x 1 = 4
4 x 2 = 8
4 x 3 = 12
5 x 1 = 5
5 x 2 = 10
5 x 3 = 15
5 x 4 = 20
6 x 1 = 6
6 x 2 = 12
6 x 3 = 18
6 x 4 = 24
6 x 5 = 30
7 x 1 = 7
7 x 2 = 14
7 x 3 = 21
7 x 4 = 28
7 x 5 = 35
7 x 6 = 42
8 x 1 = 8
8 x 2 = 16
8 x 3 = 24
8 x 4 = 32
8 x 5 = 40
8 x 6 = 48
8 x 7 = 56
9 x 1 = 9
9 x 2 = 18
9 x 3 = 27
9 x 4 = 36
9 x 5 = 45
9 x 6 = 54
9 x 7 = 63
9 x 8 = 72
```

## Explanation

Behaves similarly to exercise A, but instead only outputs
products of `i*j` in which `j<i`.

This is enforced by the line of code:

`for j in range(1,i):`

Since the `range` function excludes the range end delimiter (in this case i),
only products of `i` by `n<i` are printed.

---

# Exercise D

## Code
```python
for i in range(1,10):
    for j in range(1,i):
        z= i*j
        print i,’x’, j, ’=’, z,
        if z%2==0:
            print "par"
        else:
            print "impar"
```

## Output

```python
2 x 1 = 2 par
3 x 1 = 3 impar
3 x 2 = 6 par
4 x 1 = 4 par
4 x 2 = 8 par
4 x 3 = 12 par
5 x 1 = 5 impar
5 x 2 = 10 par
5 x 3 = 15 impar
5 x 4 = 20 par
6 x 1 = 6 par
6 x 2 = 12 par
6 x 3 = 18 par
6 x 4 = 24 par
6 x 5 = 30 par
7 x 1 = 7 impar
7 x 2 = 14 par
7 x 3 = 21 impar
7 x 4 = 28 par
7 x 5 = 35 impar
7 x 6 = 42 par
8 x 1 = 8 par
8 x 2 = 16 par
8 x 3 = 24 par
8 x 4 = 32 par
8 x 5 = 40 par
8 x 6 = 48 par
8 x 7 = 56 par
9 x 1 = 9 impar
9 x 2 = 18 par
9 x 3 = 27 impar
9 x 4 = 36 par
9 x 5 = 45 impar
9 x 6 = 54 par
9 x 7 = 63 impar
9 x 8 = 72 par
```

## Explanation

Reproduces exercise C, but this time also evaluates whether the product,
 assigned to variable `z`, is even (`par`) or odd (`impar`).

This is achieved by the [modulo function](https://docs.python.org/2/reference/expressions.html#binary-arithmetic-operations)
 and the fact that any even number should have a remainder of 0 when
 divided by 2.

Therefore, `if`

```python
# Remainder of z/2 is 0
z%2 == 0
```

the number should even.

`Else`, the number is odd.

---

# Exercise E

## Code

```python
n = input('Introduza um valor inteiro: ')
s = 0
for i in range(n+1):
    print i,s
    s = s+i
    print i,s
print 'Resultado da soma: ', s
```

## Output

```python
Introduza um valor inteiro: 5
0 0
0 0
1 0
1 1
2 1
2 3
3 3
3 6
4 6
4 10
5 10
5 15
Resultado da soma:  15
```

## Explanation

`n = input('Introduza um valor inteiro: ')`

Asks for an integer, using built-in function `input` ([Reference Docs](https://docs.python.org/2/library/functions.html#input)),
 evaluating type of given input, and assigning given value to variable `i`.

`for i in range(n+1):`

Loops variable `i` through `0...n` (by excluding `n+1`).

`print i,s`

Prints values in the current loop.

`s = s + i`

Increments variable `s` by `i` (`s += i`).

`print 'Resultado da soma: ', s`

Prints `s` after loop is done.

In short, prints `s = sum(from 0 to given value)`

For `n=5`, assigns `s` to `1+2+3+4+5=15`.

---

# Exercise F

## Code

```python
n= input("Introduza um valor inteiro: ")
s=0
i=1
while i <= n:
    print i, s
    s= s+i
    i= i+1
    print i, s
print "Resultado da soma: ", s
```

## Output

```python
Introduza um valor inteiro: 5
1 0
2 1
2 1
3 3
3 3
4 6
4 6
5 10
5 10
6 15
Resultado da soma:  15
```

## Explanation

Exact same thing as exercise E, except using a [while loop](https://wiki.python.org/moin/WhileLoop),
 substituting the line `for i in range(n+1)` by an early initialization:

`i = 1`

and then running "while" `i <= n`, making sure to increment `i` after every loop:

`i= i+1`

---

# Exercise G

## Code

```python
n= input("Introduza um valor inteiro: ")
print "o valor introduzido e ",
if n==0:
    print "zero",
elif n>0:
    print "positivo",
else:
    print "negativo",
print ":", n
```

## Output

```python
Introduza um valor inteiro: 5
o valor introduzido e  positivo : 5
```

## Explanation

Shows off `if`, `elif`, `else`.

First, asks for a value `n`, integer.

`n = n= input("Introduza um valor inteiro: ")`

Then, by switching conditional statements, evaluates whether
 the given value is positive, zero or negative.

One should note how first, `n==0` (n is 0) is evaluated,
 and only if this condition is not met, is
 `n>0` evaluated.

This is given by the `elif` (`else` + `if`) command.