# -*- coding: utf-8 -*-
def main():
    n = input('Introduza um número inteiro n: ')
    k = input('Introduza um número inteiro k: ')

    # Type checks
    if type(n) is not int:
        print "n não é inteiro"
        return
    if type(k) is not int:
        print "k não é inteiro"
        return
    
    if(n%k == 0):
        print "n é divisível por k"
        return
    
    print "n não é divisível por k"

main()