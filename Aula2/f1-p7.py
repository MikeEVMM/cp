# -*- coding: utf-8 -*-
m = input('Introduza o valor M: ')
depth = 1

def printFibToDepth(depth):
    a,b = 0,1
    for i in range(0, depth-1):
        a, b = b, a+b
    return b

fib = printFibToDepth(depth)
while fib < m:
    depth = depth + 1
    fib = printFibToDepth(depth)

print 'Fib({})={} é >= M'.format(depth, fib)

for i in range(1, depth):
    print 'Fib({})={}'.format(i, printFibToDepth(i))
