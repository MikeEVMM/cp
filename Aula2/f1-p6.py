# -*- coding: utf-8 -*-

howMany = round(input('Calcular a sequência de Fibbonacci ao termo: '))

def printFibToDepth(depth):
    a,b = 0,1
    for i in range(0, depth-1):
        a, b = b, a+b
    return b

print "F(n) = ", printFibToDepth(int(howMany))