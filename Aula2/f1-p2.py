# -*- coding: utf-8 -*-

def fatorial():
    n = input('Introduza um numero inteiro: ')

    if n < 0:
        print 'O fatorial de números negativos não é definido!'
        return
    
    if type(n) is not int:
        print 'n não é inteiro.'
        return

    givenInput = n
    fatorial = 1
    while n>0:
        fatorial = fatorial * n
        n = n-1
    print "Fatorial de {}: {}".format(givenInput, fatorial)

fatorial()