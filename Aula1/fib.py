def printFibToDepth(depth):
	a,b = 0,1
	for i in range(0, depth+1):
		print b
		a, b = b, a+b

printFibToDepth(input("Insert depth: "))

def recursiveFib(a, b, currentDepth, depth):
	a,b = b,a+b
	print b
	if currentDepth == depth:
		return "DONE"
	# Else
	return recursiveFib(a,b, currentDepth + 1, depth)

recursiveFib(0, 1, 1, input("Insert depth (calculated recursively): "))
