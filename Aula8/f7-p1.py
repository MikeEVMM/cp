# -*- coding:utf-8 -*-

from math import sqrt

tooHeavy = 0
averageWeight = 0
standardDeviation = 0

with open('luggage.txt', 'r') as inFile:
    weights = inFile.readlines()
    for weight in weights:
        averageWeight += float(weight)
        if float(weight) > 22:
            tooHeavy += 1
    
    averageWeight /= len(weights)

    dif = 0
    for weight in weights:
        dif += (averageWeight - float(weight))**2
    standardDeviation = sqrt(dif/(len(weights)-1))

print 'Average weight: {} plus or minus {}, with {} bags above 22kg.'.format(averageWeight, standardDeviation, tooHeavy)