# -*- coding:utf-8 -*-

munincipioAndInfo = []

with open('portugal2011.txt', 'r') as censusFile:
    lines = censusFile.readlines()
    labels = lines[0].strip().split(';')[:-1]
    for line in lines[1:]:
        attrs = line.strip().split(';')[:-1]
        if attrs[4] != 'Municipio':
            continue
        munincipioAndInfo.append((attrs[2], int(attrs[5]), int(attrs[27]), float(attrs[27])/float(attrs[5])))

munincipioAndInfo.sort(key=lambda x: x[3], reverse=True)
for i in range(0,20):
    info = munincipioAndInfo[i]
    print '#{}: {}, com {} edificios classicos, {} construidos entre 2006 e 2011, com uma taxa de construção de {}.'.format(
        i+1, info[0], info[1], info[2], info[3]
    )
