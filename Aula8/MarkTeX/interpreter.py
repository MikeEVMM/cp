# -*- coding:utf-8 -*-

import re

metadataTagsRe = re.compile(r'^\[(?:author|date|title):.+?\]$', re.MULTILINE | re.IGNORECASE)

'''
Returns a string with the compiled LaTeX header from source.
'''
def makeHeader(source):
    # Compiled TeX file in string format
    global compiled
    compiled = ''
    def addLine(*args):
        global compiled
        for line in args:
            compiled += line + '\n'
    addLine(r'% header start')

    # Libs to include in the TeX file.
    # Include some useful predefined ones.
    includedLibs = [
        ('inputenc','utf8'),
        ('amsmath',),
        ('amsthm',),
        ('amssymb',),
        ('natbib',),
        ('url',)
    ]

    # MDTex files are always articles, as they're meant
    #   to be notes.
    addLine('\\documentclass{article}')

    # Look for title, author, date tags.
    shouldMakeTitle = False
    metadataTags = metadataTagsRe.findall(source)
    if len(metadataTags) > 0:
       for tag in metadataTags:
        whatTag = tag[1:tag.find(':')+1]
        arg = tag[tag.find(':')+1:-1]
        if whatTag == 'title':
            addLine(r'\title{{}}').format(arg)
        elif whatTag == 'date':
            addLine(r'\title{{}}').format(arg)
        elif whatTag == 'author':
            addLine(r'\author{{}}').format(arg)

    # Check for user defined included packages
    tags = re.findall(r'^\[include:([\w\d]+)((?:,[\w\d]+)*?)\]$', source, re.MULTILINE | re.IGNORECASE)
    for tag in tags:
        newLib = []
        newLib.append(tag[0])
        if tag[1] != '':
            newLib += tag[1][1:].split(',')
        includedLibs.append(tuple(newLib))
    
    # Make library includes
    for lib in includedLibs:
        includeStr = r'\usepackage'
        if len(lib) > 1:
            includeStr += '['
            includeStr += ','.join(lib[1:])
            includeStr += ']'
        includeStr += '{{{}}}'.format(lib[0])

        addLine(includeStr)

    # Make commands (macros)
    macros = re.findall(r'^\[macro:([\w\d]+)((?:,.+?)+?)\]$', source, re.MULTILINE | re.IGNORECASE)
    argNumbers = re.compile(r'(?<!\\)#(\d)+?')
    for macro in macros:
        # Find highest argument used;
        numberOfArgs = 0
        argMatches = argNumbers.findall(macro[1])
        if len(argMatches) > 0:
            numberOfArgs = max(argMatches)
        
        addLine('\\newcommand{{{}}}[{}]{{{}}}'.format(macro[0], numberOfArgs, macro[1][1:]))

    addLine(r'% end of header')

    return compiled



def makeBody(source):
    # Compiled TeX file in string format
    global compiled
    compiled = ''
    def addLine(*args):
        global compiled
        for line in args:
            compiled += line + '\n'

    addLine('% body start')
    addLine(r'\begin{document}')

    # If there's any metadata, we should make titlepage
    if metadataTagsRe.match(source):
        addLine(r'\maketitle')

    # Remove all tags from source, to get "raw" contents
    contents = re.sub(r'^\[.+?:.+?\]$', '', source, 0, re.MULTILINE)
    contents = contents.strip()

    print contents

    addLine(r'\end{document}')

    return compiled

print makeBody(open('example.mtx').read())