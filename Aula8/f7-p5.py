# -*- coding:utf-8 -*-

cityName = raw_input('Por favor insira freguesia a procurar: ')
noneFound = True

with open('portugal2011.txt', 'r') as censusFile:
    lines = censusFile.readlines()
    labels = lines[0].strip().split(';')[:-1]
    for line in lines[1:]:
        attrs = line.strip().split(';')[:-1]
        if attrs[4] != 'Freguesia' or attrs[2].lower() != cityName.lower():
            continue
        noneFound = False
        for label,attr in zip(labels, attrs):
            print '{}: {}'.format(label, attr)
        break

if noneFound:
    print 'Nenhuma entrada encontrada para {}.'.format(cityName)