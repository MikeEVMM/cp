# -*- coding:utf-8 -*-

totalConsumption = 0
dailyAverage = 0
highestConsumption = None
highestConsumptionDay = ('0','0','0')
lowestConsumption = None
lowestConsumptionDay = ('0','0','0')
consumptionByDay = {}
residualConsumption = 0

with open('consumo.txt', 'r') as inFile:
    lines = inFile.readlines()[1:] # remove labels

    for line in lines:
        date = tuple(line[:line.find(' ')].split('/'))

        time = map(int, tuple(line[line.find(' ')+1:line.find(';')].split(':')))

        kwhConsumption = float(line[line.find(';')+1:])

        totalConsumption += kwhConsumption
        consumptionByDay[date] = consumptionByDay.get(date, 0) + kwhConsumption
        
        if time[0] >= 2:
            if time[0] < 6 or (time[0] == 6 and time[1] == 0):
                residualConsumption += kwhConsumption

    for day in consumptionByDay.keys():
        consumption = consumptionByDay[day]
        if highestConsumption is None or consumption > highestConsumption:
            highestConsumption = consumption
            highestConsumptionDay = day
        if lowestConsumption is None or consumption < lowestConsumption:
            lowestConsumptionDay = day
            lowestConsumption = consumption
        dailyAverage += consumption
    dailyAverage /= len(consumptionByDay)

print 'Daily average: {}kWh.'.format(dailyAverage)

print 'Highest consumption: {} kWh, on {}, \
a {}..% deviation on the average.'.format(
    highestConsumption,
    '/'.join(highestConsumptionDay),
    round(abs(highestConsumption-dailyAverage)/dailyAverage*100, 3)
)

print 'Lowest consumption: {} kWh, on {}, \
a {}..% deviation on the average.'.format(
    lowestConsumption,
    '/'.join(lowestConsumptionDay),
    round((lowestConsumption-dailyAverage)/dailyAverage*100, 3)
)

# Sep, 2016 started on a Wed
days = ['Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Tuesday']
for i in range(1, 7+1):
    day = i
    numberOfWeeks = 0
    consumption = 0.
    while day < 31:
        consumption += consumptionByDay[(('0' if day<10 else '') + str(day), '09', '2016')]
        day += 7
        numberOfWeeks += 1
    print 'Average consumption on {}: {}kWh. That\'s {}..% than the daily average.'.format(
        days[i-1],
        consumption/numberOfWeeks,
        round((consumption/numberOfWeeks-dailyAverage)/dailyAverage*100,3)
    )

print 'Average residual consumption: {}kWh.'.format(residualConsumption/len(consumptionByDay))