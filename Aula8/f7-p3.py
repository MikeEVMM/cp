# -*- coding:utf-8 -*-

import json
import urllib2

coin = raw_input('Please insert coin code to look for: ')
eurAmmt = input('Please insert value in EUR: ')

# Some sanitizing on coin
if len(coin) != 3:
    print 'Coin code must be 3 letters long.'
    exit()

rawJsonData = urllib2.urlopen(r'http://api.fixer.io/latest').read()
jsonData = json.loads(rawJsonData)

print '{}EUR = {}{}'.format(
    eurAmmt,
    float(jsonData['rates'][coin]) * eurAmmt if coin in jsonData['rates'] else '???',
    coin
)