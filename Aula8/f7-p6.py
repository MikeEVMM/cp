# -*- coding:utf-8 -*-

munincipioAndPopulation = []

with open('portugal2011.txt', 'r') as censusFile:
    lines = censusFile.readlines()
    labels = lines[0].strip().split(';')[:-1]
    for line in lines[1:]:
        attrs = line.strip().split(';')[:-1]
        if attrs[4] != 'Municipio':
            continue
        for label,attr in zip(labels, attrs):
            if label != 'N_INDIVIDUOS_RESIDENT':
                continue
            munincipioAndPopulation.append((attrs[2],int(attr)))
            break

munincipioAndPopulation.sort(key=lambda x: x[1], reverse=True)
for i in range(0,20):
    info = munincipioAndPopulation[i]
    print '#{}: {}, com {} residentes.'.format(i+1, info[0], info[1])