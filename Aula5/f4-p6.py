# -*- coding: utf-8 -*-

def getFebDaysByYear(year):
    if (not year%4) and (year%100 or not year%400):
        return 29
    else:
        return 28

def getMonthDays(month):
    if month == 2:
        raise ValueError('Please use getFebDaysByYears for february!')
    if month < 8:
        if not month%2: # Before june, even months have 30 days
            return 30
        else:
            return 31
    else:
        if not month%2: # After june, even months have 31 days
            return 31
        else:
            return 30

def requireValidDate(msg):
    date = input(msg)
    if type(date) is not tuple:
        print 'Invalid date.',
        print 'Please insert a tuple with 3 arguments',
        print '(day, month, year).'
        return requireValidDate(msg)
    else:
        if len(date) != 3:
            print 'Invalid date.',
            print 'Please insert a tuple with 3 arguments',
            print '(day, month, year).'
            return requireValidDate(msg)
        for arg in date:
            if type(arg) is not int:
                print 'Invalid date.',
                print 'All arguments must be given as an int;'
                print 'Jan - 1'
                print 'Feb - 2'
                print '...'
                print 'Example: 20th January 2002 = (20, 1, 2002)'
                return requireValidDate(msg)
        if date[0] < 1:
            print 'Invalid date.'
            print 'Invalid day; anti-days are not valid. (day < 0)'
            return requireValidDate(msg)
        if date[1] > 12:
            print 'Invalid date.'
            print 'Invalid month (Gregorian calendar, please).'
            return requireValidDate(msg)
        if date[2] < 1:
            print 'Invalid date.'
            print 'Invalid year; there were no PCs before christ.'
            return requireValidDate(msg)
        if date[1] == 2:
            if date[0] > getFebDaysByYear(date[2]):
                print 'Invalid date.'
                print 'Please insert a valid day/month combination.'
                return requireValidDate(msg)
        else:
            if date[0] > getMonthDays(date[1]):
                print 'Invalid date.'
                print 'Please insert a valid day/month combination.'
                return requireValidDate(msg)
    return date

date = requireValidDate('Please insert a date: ')
day = date[0]
month = date[1]
year = date[2]

haunted = 2 # Down with february

day -= 1
if day < 1:
    month -= 1
    if month < 1:
        year -= 1
        month = 12

    if month == haunted:
        day = getFebDaysByYear(year)
    else:
        day = getMonthDays(month)

print 'The day before was {}/{}/{}'.format(day, month, year)