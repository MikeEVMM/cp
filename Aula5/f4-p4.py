# -*- coding: utf-8 -*-

import time
import math

print 'Using old method (no sets).'

tick = time.clock()

m = n = 100000

howManyPrimes = 0
for i in range(2, m+1):
    prime = True
    for j in range(2, i):
        if not i%j:
            prime = False
            break
    if prime:
        howManyPrimes += 1

print 'There are {} primes between 2 and {}'.format(howManyPrimes, m)

tock = time.clock()
timeA = tock-tick
print 'That took {}s'.format(timeA)

print 'Using new method (sets).'

tick = time.clock()

primes = set()

for i in range(2, n+1):
    primes.add(i)

for j in range(2, int(math.sqrt(n))+1):
    if max(primes) < j:
        break
    for x in range(2,int((n+1)/j)+1):
        primes.discard(x*j)

print 'There are {} primes between 2 and {}'.format(len(primes), n)
tock = time.clock()
timeB = tock - tick
print 'That took {}s'.format(timeB)

print 'The second method is', 'faster' if timeB < timeA else 'slower', 'than the first method.'