# -*- coding: utf-8 -*-

def requireValidInt(msg):
    given = input(msg)
    if type(given) is not int or given<2:
        print 'Invalid int.'
        return requireValidInt(msg)
    return given

n = requireValidInt('Please input integer n>1: ')

# Get all primes between 2 and n

primes = set()

for i in range(2, n+1):
    primes.add(i)

for j in range(2, n+1):
    if max(primes) < j:
        break
    for x in range(2,int((n+1)/j)+1):
        primes.discard(x*j)

# Find prime decomposition of i

for i in range(2, n+1):
    print '{}\'s decomposition in primes is'.format(i), 
    decomp = []
    while i>1:
        for p in primes:
            if not i%p:
                i/=p
                decomp.append(str(p))
                break
    print 'x'.join(decomp)