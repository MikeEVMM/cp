# -*- coding: utf-8 -*-

def requireValidInt(msg):
    given = input(msg)
    if type(given) is not int or given<2:
        print 'Invalid int.'
        return requireValidInt(msg)
    return given

n = requireValidInt('Please input integer n>1: ')

primes = set()

for i in range(2, n+1):
    primes.add(i)

for j in range(2, n+1):
    if max(primes) < j:
        break
    for x in range(2,int((n+1)/j)+1):
        primes.discard(x*j)

print 'Primes from 2 to {}:'.format(n)
print primes