# -*- coding: utf-8 -*-

inputString = raw_input('Please input a string: ')

charDict = {}

for char in inputString:
    charDict[char] = charDict.get(char, 0) + 1

maxValue = 0
maxKey = None
for char in charDict.keys():
    relative = float(charDict[char])/len(inputString)*100
    charDict[char] = relative
    if(relative > maxValue):
        maxKey = char
        maxValue = relative

print 'The most occurring letter is {}, with a frequency of {}%'.format(maxKey, maxValue)