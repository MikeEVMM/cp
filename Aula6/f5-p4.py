# -*- coding:utf-8 -*-

import math

a = input('Insert coefficient a: ')*1.
b = input('Insert coefficient b: ')*1.
c = input('Inser coefficient c: ')*1.

delta = b**2-4*a*c

if delta < 0:
    sqrt = complex(0, math.sqrt(abs(delta)))
else:
    sqrt = math.sqrt(delta)

print 'x = {} or x = {}'.format((-b+sqrt)/(2*a), (-b-sqrt)/(2*a))
