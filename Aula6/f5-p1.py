#-*- coding:utf-8 -*-

def requireInt(msg):
    given=input(msg)
    if type(given) is not int:
        print 'Please insert int.'
        return requireInt(msg)
    return given

n = requireInt('Insert integer n: ')
mul=1

for i in range(3, n):
    if i%2:
        mul*=i

print 'The product of all \
odd numbers smaller than {} \
is {}'.format(n, mul)
