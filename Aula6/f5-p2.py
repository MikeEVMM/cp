# -*- coding:utf-8 -*-

def sumOfDigits(num):
    sum = 0
    for char in str(num):
        sum += int(char)
    return sum

n = input('Please insert integer \
n: ')

sumA = sumOfDigits(n)
sumB = sumOfDigits(sumA)

print sumA
print sumB
