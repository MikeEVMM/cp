# -*- coding:utf-8 -*-

from random import randint

def prob(i):
    return (6. if i==7 else (5./abs(7-i)))/36

absoluteValues = {}
throws = input('Please insert n. of throws: ')

for i in range(0, throws):
    throwA = randint(1,6)
    throwB = randint(1,6)
    absoluteValues[throwA+throwB] = absoluteValues.get(throwA+throwB, 0)+1

for i in range(2, 12+1):
    relative = float(absoluteValues.get(i, 0))/throws
    deviation = relative/prob(i)-1 if relative != 0 else prob(i)
    print '''Relative frequency: {}
Expected frequency: {}
Error: {}'''.format(relative, prob(i), deviation)
