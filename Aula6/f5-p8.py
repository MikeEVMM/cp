# -*- coding:utf-8 -*-

polygons = {
    3:'triangle',
    4:'square',
    5:'pentagon',
    6:'hexagon',
    7:'heptagon',
    8:'octagon',
    9:'eneagon',
    10:'decagon',
    11:'hendecagon',
    12:'dodecagon'
}

import random
sides = int(random.uniform(3,13))
print 'How many sides does the {} have?'.format(polygons[sides])
answer = input('Answer: ')
if answer == sides:
    print 'Correct.'
else:
    print 'Wrong.'
