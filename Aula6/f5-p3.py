# -*- coding:utf-8 -*-

def getTri(n):
    return n*(n+1)/2

def greatestTri(t):
    lastIndex = 1
    while getTri(lastIndex+1)<=t:
        lastIndex+=1
    return (lastIndex, getTri(lastIndex))

given = input('Please insert int\
eger t: ')

if given <= 0:
    print 'T(0)=0'
else:
    result = greatestTri(given)
    
    print 'T({})={} is the greatest \
triangular number smaller or equal \
to {}.'.format(result[0], result\
[1], given)
