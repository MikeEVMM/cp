# -*- coding:utf-8 -*-

from random import randint

absoluteValues = {}
throws = 100
for i in range(0,throws):
    throwA = randint(1,6)
    throwB = randint(1,6)
    absoluteValues[throwA+throwB] = absoluteValues.get(\
throwA+throwB, 0)+1

for i in range(1, 12+1):
    print 'Relative occurence of {}:{}'.format(\
i, float(absoluteValues.get(i, 0))/throws)
