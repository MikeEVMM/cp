# -*- coding: UTF-8 -*-

def main():
    n = input("Input n integer: ")
    k = input("Input k integer: ")

    if type(n) is not int:
        print "n is not an integer."
        return
    if type(k) is not int:
        print "k is not an integer."
        return
    
    divisible = []
    for i in range(1, n+1):
        if not i%k:
            divisible.append(i)
    print 'The {} {} {} divisible by {}'.format('numbers' if len(divisible)-1 else 'number', str(divisible).replace('[', '').replace(']', ''), 'are' if len(divisible)-1 else 'is', k)

main()