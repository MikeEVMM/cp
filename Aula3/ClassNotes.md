# User Input/Output

In programming, it is best to detach an algorithm from the initial conditions, having these, instead, be input by the user.
 This increases code reusability.

As such, we need a method to communicate with the user, in order to obtain the values.

---
## input

In python, this can be done through the `input` methdod:

```python
>>> s = input("Enter your name: ")
Enter your name: "Helmut"
>>> s
'Helmut'
```

In this case, we can see that we ask for a value (using the prompt `Enter your name: `), and assign it to `s`.

Calling `s`, python echos its value, showing that `s` now has the value of `Helmut` (in this case).

---
## raw_input

In using `input`, python automatically verifies the given expression.

This means that, for instance, inputting

```python
>>> s = input("Insert value: ")
Insert value: 5*7
>>> s
35
```

assigns to s the parsed value of the given input.

This might not be convinient at all times, so `raw_input` can be used.

This method takes the given input as pure, interpreting it as a string.

For instance:

```python
>>> s = raw_input("Insert data: ")
Insert data: 57
>>> s*2
5757
```

As you can see in the above example, `s`, despite being assigned `57`, is interpreted as a string,
 such that `s*2` yields the string repeated twice.

This can be combined with casting methods (`int(s)`) to convert between types, but this is a more
 advanced subject, and besides the current scope.

---

# Conditionals

In python (much like in almost any programming language), one core element of programming are the
 conditional evaluations.

This is better observed in practice:

```python
if a == b:
    print "a equals b"
elif a >= b:
    print "a greater or equal to b"
else:
    print "b smaller than a"
```

In the above example, you can see the use of 2 boolean operators and 3 important
 keywords in conditional assessment.

---

## Boolean operators

The `==` operand assesses whether the expression to its left equals in value
 to the expression to its right.

In practice:

`a == b`

is equivalent to the boolean value `True` if `a` has the same value as `b`,
 or equivalent to the boolean value `False` if `a` has a different value from `b`.

Similarly to `==`, operators such as `>`, `<`, `>=`, `<=` and `!=`, evaluate
 whether the left side is, respecively,

. greater,

. smaller,

. greater or equal to,

. smaller or equal to,

. different to

 the expression to the right.

Any of these operators is substituted by the equivalent boolean value, `True` or `False`.

---
#### Note:

Some variable values also have an equivalent Boolean value.

Specifically, `0` has a value of `False` and `1` has a value of `True`.

Any non empty `string` has a boolean value of `True`.
 An empty string has a boolean value of `False`.

---
### Example:

```python
if True:
    print "This conditional statement is always true"
if False:
    print "This conditional statement is always false"
print (1 == 1)
```

yields

```python
"This conditional statement is always true"
True
```

---

## if

The `if` command evaluates an expression (such as an equallity, inequallity, ...),
 and runs sections of code depending on the resulting logic value.

Its intuitiveness has allowed us to use it in previous examples:

```python
if <Boolean Value>:
    <Code that runs iff Boolean Value is True>
```

## else

The `else` keyword must always come with a previous `if` keyword.

Its code is ran iff the previous evaluated `if` statement is not met.

For example:

```python
if False:
    print "This is not print."
else:
    print "This is always print."
```

## elif

The `elif` keyword, as indicated by its name, is a combination of the
 `else` and `if` keyword.

Like the `else` keyword, it must always come after an `if` statement,
 or another `elif` statement.

If the previous condition is not met, the code under `elif` runs
 iff `elif`'s statement is `True`.

This keyword can be combined with `else`.

For example:

```python
if False:
    print "This is never print"
elif True:
    print "This is print, because the previous value is False"
else
    print "This is not print, because the previous value is True"
```

---

# Loops

Much like the `input` command allows you to write more abstract and reusable code,
 loops allow you to avoid manually writing similar code multiple times,
 instead running the same method with abstract values.

For example, instead of writing

```python
print 1
print 2
print 3
print 4
    .
    .
    .
print 100
```

using loops you could write

```python
for i in range(1, 101):
    print i
```

which is obviously much more convenient.

---

## for

The for keyword loops over an iteratable method. Although it is, for now,
 out of scope to discuss what an iteratable object is,
 it can be loosely (and circularly) defined as something
 containing individual elements to go over.

Like all previous subjects, this is much more observable in practice:

```python
for i in [1, 5, "some text"]:
    print i
```

yields

```
1
5
some text
```

(We are here using the notion of list, not yet introduced, but 
 it can be compared to an ordered mathematical set.)

using the `for ... in` keywords, we can also loop over the individual characters
 of a string:

```python
for letter in "some text":
    print letter
```

yields

```
s
o
m
e

t
e
x
t
```

We can also loop over a range of numbers, but for that we must introduce the `range`
 function.

---
## range

The range function defines an iteratable object that goes from a starting number
 to an ending number (excluding), moving a delta value at a time.

It is defined as follows:

```python
range(start, end, delta)
```

where `start` and `delta` are optional arguments, having the default value of,
 respecively, `0` and `1`.

This function is, again, best grasped in example:

```python
for i in range(0,5,1):
    print i
```

yields

```
0
1
2
3
4
```

```python
for i in range(5):
    print i
```

yields

```
0
1
2
3
4
```

```python
for i in range(3, 6):
    print i
```

yields

```
3
4
5
```

and finally,

```python
for i in range(1, 10, 2):
    print i
```

yields

```
1
3
5
7
9
```

---
#### Note

The `range` function contains the whole of the range in memory at all times
 (since it is faster.)

This means that, for a range of 10.000.000, with each number at 4 bytes,
 range uses up 40.000.000 bytes, or 4GB of ram.

As such, the slower, but memory optimal function `xrange` exists,
 for very large ranges.

---

## Example

As a practical use of loops, observe the following implementation
 of a sum of `i`, from `0` to a given value.

```python
n = int(input("Sum from 0 to: "))

sum = 0
for i in range(0, n+1):
    sum = sum + i

print sum
```