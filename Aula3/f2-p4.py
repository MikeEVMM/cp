# -*- coding: UTF-8 -*-

def main():
    m = input('Input positive integer m: ')

    if type(m) is not int or not m>0:
        print 'Invalid m.'
        return main()
    
    for i in range(2, m+1):
        prime = True
        for j in range(2, i):
            if not i%j:
                prime = False
                break
        print i, 'is' if prime else 'isn\'t', 'prime'
    
main()