# -*- coding: UTF-8 -*-

def main():
    n = input("Input positive integer n: ")

    if type(n) is not int or not n>0:
        print 'Invalid n.'
        return main()
    
    print 'n is divisible by',

    for k in range(1, n):
        if not n%k:
            print k,

main()