# -*- coding: UTF-8 -*-

def main():
    m = input('Input positive integer m: ')

    if type(m) is not int or not m>0:
        print 'Invalid m.'
        return main()
    
    howManyPrimes = 0
    for i in range(2, m+1):
        prime = True
        for j in range(2, i):
            if not i%j:
                prime = False
                break
        if prime:
            howManyPrimes += 1
    
    print 'There are {} primes between 2 and {}'.format(howManyPrimes, m)
    
main()