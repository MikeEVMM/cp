# -*- coding: UTF-8 -*-

import math

def approxPi(n):
    piApprox = 1

    for i in range(1, n+1):
        piApprox *= float((2*i)**2)/float((2*i)**2-1)
    
    return piApprox

def main():
    n = input('Input a positive integer n: ')

    if type(n) is not int or not n>0:
        print 'Invalid n.'
        return main()
    
    print 'Pi/2 is approximately {}, with {} iterations.'.format(approxPi(n), n)

    minPrecision = 1
    while math.pi/2-approxPi(minPrecision) > 0.0001:
        minPrecision += 1
    
    print 'To get a precision of 0.0001, pi/2 must be approximated with {} iterations.'.format(minPrecision)

main()