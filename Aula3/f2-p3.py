# -*- coding: UTF-8 -*-

def main():
    n = input('Input positive integer n: ')

    if type(n) is not int or not n>0:
        print 'invalid n.'
        return main()
    
    prime = True

    for i in range(2, n):
        if not n%i:
            prime = False
            print '{} is divisible by {}'.format(n, i)
            break
    
    print 'n', 'is' if prime else 'isn\'t', 'prime'

main()