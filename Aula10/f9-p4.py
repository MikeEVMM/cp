import sys

try:
  assert sys.version_info >= (3,0)
except:
  sys.stderr.write("This is a python3 script.\n")
  exit()

import graphics
win = graphics.GraphWin()
shape = graphics.Rectangle(graphics.Point(50-20,50-20), graphics.Point(50+20, 50+20))
shape.setOutline("red")
shape.setFill("red")
shape.draw(win)
for i in range(10):
  p = win.getMouse()
  c = shape.getCenter()
  dx = p.getX() - c.getX()
  dy = p.getY() - c.getY()
  newShape = shape.clone()
  newShape.draw(win)
  newShape.move(dx,dy)

txt = graphics.Text(graphics.Point(win.width/2,win.height/2), 'Nice drawing!\nClick to quit.')
txt.setSize(16)
txt.draw(win)

win.getMouse()
win.close()
