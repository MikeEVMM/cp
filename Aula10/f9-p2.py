import sys

try:
  assert sys.version_info >= (3,0)
except:
  sys.stderr.write("This is a python3 script.\n")
  exit()

import random
from graphics import *

# Logic bit
results = [0 for i in range(2,12+1)]

nOfTosses = 10000
for toss in range(nOfTosses):
  results[random.randint(1,6) + random.randint(1,6) - 2] += 1

# Graphics bit

# Constants
width = 640
height = 480
margin = 30 

# Window
window = GraphWin('Relative Frequency', width+margin*2, height+margin*2)

# Draw vertical axis 
vAxis = Line(Point(margin, margin), Point(margin, margin + height))
vAxis.setArrow('last') 
vAxis.draw(window)

# Vertical Axis label
vAxisLabel = Text(Point(margin + 30, height + margin), 'Frequency')
vAxisLabel.draw(window)

# Draw horizontal axis
hAxis = Line(Point(margin, margin), Point(margin + width,margin))
hAxis.setArrow('last')
hAxis.draw(window)

# Horizontal axis label
hAxisLabel = Text(Point(margin + width - 15, margin + 10), 'Sum')
hAxisLabel.draw(window)

# Vertical value marks
for i in range(100):
  atH = margin + height / 100 * i
  Line(Point(margin - 2, atH), Point(margin + 2, atH)).draw(window)

# Draw histogram lines
axisMargin = 40
for i in range(0, 11):
  rectW = (width - axisMargin*2) / len(results)
  xOffset = rectW * i + rectW/2 + axisMargin
  yValue = results[i] / nOfTosses * height
  
  histLine = Line(Point(margin + xOffset, margin), Point(margin + xOffset, margin + yValue))
  histLine.setOutline('red')
  histLine.draw(window)

  histRect = Rectangle(Point(margin + xOffset - rectW / 2, margin), Point(margin + xOffset + rectW /2, margin + yValue))
  histRect.draw(window)
  
  # Add label at base
  Line(Point(margin + xOffset, margin - 2), Point(margin + xOffset, margin + 2)).draw(window)
  Text(Point(margin + xOffset, margin - 10), i+2).draw(window)

window.getMouse()
window.close()
