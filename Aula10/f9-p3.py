import sys

try:
  assert sys.version_info >= (3,0)
except:
  sys.stderr.write("This is a python3 script.\n")
  exit()

from graphics import *
from math import pi, cos, sin

# User input
sides = int(input('Please insert number of polygon sides: '))

# Constants
width = 640
height = 480
margin = 5

# Prepare graphical env
window = GraphWin('Polygon', width, height)

# Circle
radius = min(width/2 - margin, height/2 - margin) 
circle = Circle(Point(width/2, height/2), radius)
circle.draw(window)

# Draw polygon
circleCenter = circle.getCenter()
for i in range(sides):
  pointA = Point(circleCenter.x + cos(((i - 1) / sides * 360) / 180 * pi) * radius,\
                 circleCenter.y + sin(((i - 1) / sides * 360) / 180 * pi) * radius)
  pointB = Point(circleCenter.x + cos((i / sides * 360) / 180 * pi) * radius,\
                 circleCenter.y + sin((i / sides * 360) / 180 * pi) * radius)
  Line(pointA, pointB).draw(window)

# Close up
window.getMouse()
window.close()
