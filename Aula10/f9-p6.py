import sys

try:
  assert sys.version_info >= (3,0)
except:
  sys.stderr.write("This is a python3 script.\n")
  exit()

from graphics import *

win = GraphWin('Draw a rectangle with two clicks',1000,500)

pointA = win.getMouse() 
pointB = win.getMouse()

rect = Rectangle(pointA, pointB)
rect.draw(win)

perimeter = 2 * abs(pointA.x - pointB.x) + 2 * abs(pointA.y - pointB.y)
area = abs(pointA.x - pointB.x) * abs(pointA.y - pointB.y)

Text(rect.getCenter(), 'Area:{}\nPerimeter:{}'.format(area, perimeter)).draw(win)

win.getMouse()
win.close()
