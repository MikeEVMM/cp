import sys

try:
  assert sys.version_info >= (3,0)
except:
  sys.stderr.write("This is a python3 script.\n")
  exit()

from graphics import *
import time
from math import sin, cos, pi

# Constants
width = 640
height = 480
margin = 5
updateEverySec = 0.5

# Prepare graphics
# Window
window = GraphWin('The Time', width, height)

# Circle
radius = min(width/2 -2*margin, height/2 - 2*margin)
circle = Circle(Point(width/2, height/2), radius)
circle.draw(window)
circleCenter = circle.getCenter()

# Handle sizes
minHandleSize = radius * 5/6
hrHandleSize = radius * 3/5
secHandleSize = radius * 7/8

# Draw cycle
def draw():
  clock = time.localtime(time.time())
  hour = clock.tm_hour
  if hour > 12:
    hour -= 12
  minutes = clock.tm_min
  secs = clock.tm_sec
  hourAngle = hour / 6 * pi - pi/2
  minutesAngle = minutes / 30 * pi - pi/2
  secAngle = secs / 30 * pi - pi/2
  try:
    draw.secHandle.undraw()
    draw.minHandle.undraw()
    draw.hourHandle.undraw()
  except:
    pass
  draw.secHandle = Line(circleCenter, Point(circleCenter.x + cos(secAngle) * secHandleSize, circleCenter.y + sin(secAngle) * secHandleSize))
  draw.minHandle = Line(circleCenter, Point(circleCenter.x + cos(minutesAngle) * minHandleSize, circleCenter.y + sin(minutesAngle) * minHandleSize))
  draw.hourHandle = Line(circleCenter, Point(circleCenter.x + cos(hourAngle) * hrHandleSize, circleCenter.y + sin(hourAngle) * hrHandleSize))
  
  draw.secHandle.setOutline('red')

  draw.secHandle.draw(window)
  draw.minHandle.draw(window)
  draw.hourHandle.draw(window)

try:
  while True:
    draw()
    for i in range(10):
      if window.checkMouse() is not None:
        raise Exception() # ʰᵃᶜᵏ
      time.sleep(0.1)
except:
  pass
window.close()
