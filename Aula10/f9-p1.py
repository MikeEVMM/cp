import sys

try:
  assert sys.version_info >= (3,0)
except:
  sys.stderr.write("This is a python3 script.\n")
  exit()

from graphics import * 

# Constants
nHorizSqr = 8
nVertSqr = 8
sqrSide = 80
width = sqrSide * nHorizSqr
height = sqrSide * nVertSqr
margin = 5

# Initialize window
window = GraphWin("Chess Board", width+2*margin, height+2*margin)
canvas = Rectangle(Point(margin, margin), Point(width+margin, height+margin))
canvas.draw(window)

sqrW = width / nHorizSqr
sqrH = height / nVertSqr
fill = 0 
for x in range(nHorizSqr):
  rectX = width/nHorizSqr * x + margin
  for y in range(nVertSqr):
    rectY = height/nVertSqr * y + margin
    fill = abs(fill - 255)
    square = Rectangle(Point(rectX, rectY), Point(rectX + sqrW, rectY + sqrH))
    square.setFill(color_rgb(fill, fill, fill))
    square.draw(window)
  fill =abs(fill- 255) 

window.getMouse()
window.close()
