import sys

try:
  assert sys.version_info >= (3,0)
except:
  sys.stderr.write("This is a python3 script.\n")
  exit()

from graphics import *

win = GraphWin('Draw a segment with two clicks',1000,500)

pointA = win.getMouse() 
pointB = win.getMouse()

line = Line(pointA, pointB)
line.draw(win)

circle = Circle(line.getCenter(), 2)
circle.setOutline('red')
circle.draw(win)

win.getMouse()
win.close()
