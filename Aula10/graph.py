#from graphics import *
import graphics
import random

width = 500
height = 500
margin = 20
xorig = width/2+margin
yorig = height/2+margin
radius = 200
multip = 250.0

win = graphics.GraphWin("My Circle", width+2*margin, height+2*margin)
c = graphics.Rectangle(graphics.Point(margin,margin), graphics.Point(width+margin,height+margin))
c.draw(win)
c = graphics.Circle(graphics.Point(xorig,yorig), radius)
c.setOutline('green')
c.setWidth(2)
c.draw(win)
for i in range(1000):
  x = random.gauss(0.0,1.0)
  y = random.gauss(0.0,1.0)
#  x=y=-1.0+i/50.0
  xc = width/2+margin+multip*x
  yc = height+margin-(height/2+multip*y)
  if xc > margin and xc < width+margin and yc > margin and yc < height+margin:
    gpt = graphics.Point(xc,yc)
    c = graphics.Circle(gpt, 2)
    if x**2 + y**2 <= (radius/multip)**2:
      c.setFill('green')
    else:
      c.setFill('blue')
    c.draw(win)
win.getMouse() # pause for click in window
win.close()

